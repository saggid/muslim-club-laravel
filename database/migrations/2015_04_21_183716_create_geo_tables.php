<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('geo_countries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('iso', 2);
			$table->string('code_phone', 10)->nullable();
      $table->unique('name');
      $table->timestamps();
		});
    
		Schema::create('geo_regions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->integer('country_id');
      $table->foreign('country_id')->references('id')->on('geo_countries');
      $table->unique(['name','country_id']);
      $table->timestamps();
		});
    
		Schema::create('geo_cities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('area');
			$table->string('shirota', 9);
			$table->string('dolgota', 9);
			$table->integer('country_id');
      $table->foreign('country_id')->references('id')->on('geo_countries');
			$table->integer('region_id')->nullable();
      $table->foreign('region_id')->references('id')->on('geo_regions');
      $table->index('name');
      $table->timestamps();
		});
    
    Schema::table('users', function(Blueprint $table) {
        $table->integer('city_id');
        $table->foreign('city_id')->references('id')->on('geo_cities');
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
    Schema::table('users', function(Blueprint $table) {
        $table->dropColumn('city_id');
    });
		Schema::drop('geo_cities');
		Schema::drop('geo_regions');
		Schema::drop('geo_countries');
	}

}

Schema::table('users', function($table) {
        $table->removeColumn('city_id');
    });