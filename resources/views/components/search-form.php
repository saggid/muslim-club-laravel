<?php
    if (!isset($placeholder)) {
        $placeholder = 'Поиск..';
    }
?>

<input
    type="text"
    id="search-input"
    name="search"
    class="pull-right"
    style="width: 300px"
    placeholder="<?=$placeholder?>"
    value="<?=e(array_get($_GET,'search'))?>"
/>
<script>
    App.onFullLoad(function() {
        $('#search-input').on('keypress', function(event) {
            if (event.keyCode === 13) {
                var query = $(event.currentTarget).val();
                reloadWithGetParam('search',query,true);
            }

        });
        if ($('#search-input').val()) {
            $('#search-input').focus();
        }
    });
</script>