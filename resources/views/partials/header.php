<?php
    if (!isset($user))        { $user = new User;   }
    if (!isset($description)) { $description = '';  }
    if (!isset($keywords))    { $keywords = '';     }
?><!DOCTYPE html>
<html>
<head>
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>" />
  <meta name="keywords" content="<?=$keywords?>" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <link rel="shortcut icon" href="/favicon.ico?16" />
  <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <?php include 'assets_list_header.php'; ?>
</head>
<body class="flex-site">

  <nav class="top-bar" data-topbar>
    <ul class="title-area">
      <li class="name">
        <h1><a href="/">Всегда рядом</a></h1>
      </li>
      <li class="toggle-topbar menu-icon"><a href="#">Меню</a></li>
    </ul>

    <section class="top-bar-section">
      <ul>
        <? if (\Auth::check()) { ?>
          <li> <a href="<?=$user->link()?>"> <i class="fa fa-user"></i> <?=$user->name()?></a> </li>
          <li> <a href="<?=route('messages')?>"> <i class="fa fa-list-ul"></i> Сообщения</a> </li>
          <? if ($user->isAdmin()) { ?> <li> <a href="<?=url('admin')?>"> <i class="fa fa-gear"></i> Админка</a> </li> <?php } ?>
        <? } ?>
        <li> <a href="<?=url('search')?>"> <i class="fa fa-users"></i> Поиск дузей</a> </li>
      </ul>
      <ul class="right">
        <? if (\Auth::check()) { ?>
          <li> <a data-navigation="base" href="<?=url('auth/logout')?>">Выйти</a> </li>
        <? } else { ?>
          <li> <a href="<?=url('auth/login')?>">Войти</a> </li>
          <li> <a href="<?=url('auth/register')?>">Зарегистрироваться</a> </li>
        <? } ?>
      </ul>
    </section>
  </nav>
