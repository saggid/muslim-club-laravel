<div class="panel footer">
  <div class="Grid">
    <div class="Grid-cell">© Ramadan Software</div>
    <div class="Grid-cell Grid--bottom">
      <ul class="inline-list right">
        <li><a href="/about">О проекте</a></li>
        <li><a href="/contacts">Контакты</a></li>
      </ul>
    </div>
  </div>
</div>

<?php include 'assets_list_footer.php'; ?>

<script>
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': '<?= csrf_token() ?>'
    }
  });
</script>

</body>
</html>