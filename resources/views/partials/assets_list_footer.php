<?php if(Config::get('app.debug')): ?>
  <?php if(isset($params->js_foot)): ?>
    <?php foreach($params->js_foot as $jsfile) {
      if (mb_strpos($jsfile,'.tag') !== FALSE) { ?>
        <script src="<?=$params->path_compiled?>debugging/<?=exclude_ext($jsfile)?>.js"></script>
      <?php } else { ?>
        <script src="<?=$params->path_dev?><?=$jsfile?>"></script>
      <?php }
    } ?>
  <?php endif; ?>
<?php else : ?>
  <?php if(isset($params->js_foot)): ?>
    <script src="<?=$params->path_compiled?>application_footer.js?<?=asset_timestamp('application_footer.js')?>"></script>
  <?php endif; ?>
<?php endif; ?>
