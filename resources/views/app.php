<?php include 'partials/header.php'; ?>

<div class="row content-container">
  <div class="small-12 columns" id="content">
    <?php echo $__env->yieldContent('content'); ?>
    <?php if (isset($content)) { echo $content; } ?>
  </div>
</div>

<?php include 'partials/footer.php';