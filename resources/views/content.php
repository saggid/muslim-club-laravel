<?php

use App\Helpers\Params;

/**
 * Данный файл представляет из себя основной контент страницы, он всегда передаётся клиенту
 * Если это был обычный запрос, то данный контент обернётся в шаблон.
 * Если это был AJAX-запрос, то этот контент не будет ничем оборачиваться.
 */

$params = Params::instance();
$breadcrumbs_routes = $params->breadcrumbs_routes;
$breadcrumbs_hidden = $params->breadcrumbs_hidden;
$route_name = Route::getCurrentRoute()->getName();

// Если текущий роут не находится в списке скрытых крошек,
// попробуем отобразить крошки
if (! in_array($route_name, $breadcrumbs_hidden)) {
    echo Breadcrumbs::renderIfExists();
}

if ($messages = displayMessages()) {
    echo $messages;
}

echo $html;

if (!isset($description)) {
    $description = '';
}

// При аякс-операции добавляем команду установки метаданных страницы
if ($ajaxop) { ?>
<script>
    Navigation.setTitle('<?=e($title)?>','<?=e($description)?>');
</script>
<?php }