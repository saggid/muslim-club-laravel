
<h1>Поиск людей</h1>

<div class="row">

    <table style="width: 100%">
        <thead>
            <tr>
                <th>Имя</th>
                <th>Место жительства</th>
                <th>Интересы</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach($persons as $person) { ?>
          <tr>
              <td> <?= $person->htmlLink() ?> </td>
              <td>
                  <span data-tooltip aria-haspopup="true" class="locality has-tip tip-bottom radius"
                      title="<?=$person->city->region->country?>, <?=$person->city->region?>"><?=$person->city?></span>
              </td>
              <td>
                  <?php foreach($person->hobbies as $hobby) { ?>
                      <span class="label"><?=$hobby->title?></span>
                  <?php } ?>
              </td>
          </tr>
          <?php } ?>
        </tbody>
    </table>

</div>