<?php

use App\Helpers\Text;

?>

<h1>Пользователи</h1>

<?=view('components/search-form',[ 'placeholder' => 'Поиск по имени, email или телефону..' ])?>
<br/><br/>

<?=$persons->appends(Input::except('page'))->render()?>
<table style="width: 100%;">
    <thead>
        <tr>
            <th class="clickable tablesorter-order" data-order="id">№</th>
            <th class="clickable tablesorter-order" data-order="fio">Имя</th>
            <th class="clickable tablesorter-order" data-order="created_at">Создан</th>
            <th class="clickable tablesorter-order" data-order="city_id">Город</th>
            <th>Детали</th>
            <th width="100">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($persons as $person) { ?>
        <tr<?php if ($person->trashed()) { ?> class="text-transparent" <?php } ?>>
            <td><?=$person->id?></td>
            <td>
                <?=$person->name?>
            </td>
            <td> <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="изменён <?=Text::humanDate($person->updated_at)?>"> <?=Text::humanDate($person->created_at)?> </span> </td>
            <td> <span data-tooltip aria-haspopup="true" 
                title="<?=$person->city->region->country?>, <?=$person->city->region?>"
                class="has-tip tip-bottom"> <?=$person->city?> </span> </td>
            <td>
                <?php if ($person->trashed()) { ?>
                <span class="label label-info">Удалён <?=Text::humanDate($person->deleted_at)?></span>
                <?php } ?>
                <?php if ($person->isAdmin()) { ?>
                <span class="label">Админ</span>
                <?php } ?>
            </td>
            <td class="right">
                <?php if ($person->trashed()) { ?>
                    <div data-link='<?=route('admin.users')?>/restore/<?=$person->id?>' class='button tiny senddata'>восстановить</div>
                    <div data-link='<?=route('admin.users')?>/force-delete/<?=$person->id?>'
                         data-really-text-btn="Удалить пользователя"
                         data-really-text="Вы действительно желаете полностью удалить пользователя из системы? Это действие необрабимо."
                         class='button tiny really senddata'>удалить окончательно</div>
                <?php } else { ?>
                    
                    <button href="#" data-dropdown="drop<?=$person->id?>" aria-controls="drop<?=$person->id?>" aria-expanded="false" class="button tiny dropdown"> <i class="fa fa-pencil-square-o"></i> </button>
                    <br>
                    <ul id="drop<?=$person->id?>" data-dropdown-content class="f-dropdown" aria-hidden="true">
                      <li> <a href='<?=route('admin.users')?>/password/set/<?=$person->id?>'>пароль</a> </li>
                      <li> <a href='<?=route('admin.users.save', $person->id)?>'>редактировать</a> </li>
                      <li> <a href="#" data-link='<?=route('admin.users')?>/delete/<?=$person->id?>'
                         data-really-text-btn="Удалить пользователя"
                         data-really-text="Вы действительно желаете удалить пользователя из системы?"
                         class='senddata really'>удалить</a> </li>
                    </ul>
                    
                <?php } ?>
            </td>
        </tr>
        <?php } if (empty($persons->count())) { ?>
        <tr>
            <td colspan='99'>В базе еще нет ни одного пользователя</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?=$persons->appends(Input::except('page'))->render()?>
