<?php

use App\Helpers\Text;

?>

<h1>Хобби</h1>

<a href="<?=route('admin.hobbies.save')?>" class="button tiny">Новое хобби</a>
<?=view('components/search-form',[ 'placeholder' => 'Поиск по имени...' ])?>
<br/><br/>

<?=$hobbies->appends(Input::except('page'))->render()?>
<table style="width: 100%;">
    <thead>
        <tr>
            <th class="clickable tablesorter-order" data-order="id">№</th>
            <th class="clickable tablesorter-order" data-order="fio">Наименование</th>
            <th class="clickable tablesorter-order" data-order="created_at">Создан</th>
            <th width="100">Действия</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($hobbies as $hobbie) { ?>
        <tr<?php if ($hobbie->trashed()) { ?> class="text-transparent" <?php } ?>>
            <td><?=$hobbie->id?></td>
            <td><?=$hobbie->title?></td>
            <td title="изменён <?=Text::humanDate($hobbie->updated_at)?>"><?=Text::humanDate($hobbie->created_at)?></td>
            <td class="right">
                <?php if ($hobbie->trashed()) { ?>
                    <div data-link='<?=route('admin.hobbies')?>/restore/<?=$hobbie->id?>' class='button tiny btn-xs senddata'>восстановить</div>
                    <div data-link='<?=route('admin.hobbies')?>/force-delete/<?=$hobbie->id?>'
                         data-really-text-btn="Удалить хобби"
                         data-really-text="Вы действительно желаете полностью удалить хобби из системы? Это действие необрабимо."
                         class='button tiny btn-xs really senddata'>удалить окончательно</div>
                <?php } else { ?>
                    
                    <button href="#" data-dropdown="drop<?=$hobbie->id?>" aria-controls="drop<?=$hobbie->id?>" aria-expanded="false" class="button tiny dropdown"> <i class="fa fa-pencil-square-o"></i> </button>
                    <br>
                    <ul id="drop<?=$hobbie->id?>" data-dropdown-content class="f-dropdown" aria-hidden="true">
                      <li> <a href='<?=route('admin.hobbies.save', $hobbie->id)?>'>редактировать</a> </li>
                      <li> <a href="#" data-link='<?=route('admin.hobbies')?>/delete/<?=$hobbie->id?>' class='senddata'>удалить</a> </li>
                    </ul>
                    
                <?php } ?>
            </td>
        </tr>
        <?php } if (empty($hobbies->count())) { ?>
        <tr>
            <td colspan='99'>В базе еще нет ни одного хобби</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?=$hobbies->appends(Input::except('page'))->render()?>
