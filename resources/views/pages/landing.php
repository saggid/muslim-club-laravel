<div class="landing">
  <div class="row main-landing-title">
    <div class="large-12 columns">
      <h1>~ Всегда рядом ~</h1>
      <p class="serif muted">Социальная сеть, объединяющая мусульман по интересам</p>
    </div>
  </div>
  <div class="row">
    <div class="small-12 medium-8 columns">
      <h3 class="text-left">Найдите новых друзей в своём регионе</h3>
      <p>
        Сколько братьев и сестёр живёт недалеко от вас, о существовании которых вы даже не подозреваете?
        Найдите их с помощью нашего сервиса и предоставье им возможность найти вас!
      </p>
      <a href="/search" class="button success center-btn">Найти друзей!</a>
    </div>
      <div class="small-12 medium-4 columns text-center">
          <div class="landing-img landing-img-1"></div>
      </div>
  </div>

  <hr>

  <div class="row">
    <div class="large-12 columns">
      <h3>сервис, полезный всем</h3>
    </div>
  </div>
  <div class="row features">
    <div class="small-12 medium-4 columns text-center">
      <div class="landing-img landing-img-2"></div>
      <h4>Мамам и их малышам</h4>
      <p>
        У вас есть дети, и вы бы хотели найти для них друзей? Найдите их уже сегодня с помощью нашего сервиса.
      </p>
    </div>
    <div class="small-12 medium-4 columns text-center">
      <div class="landing-img landing-img-3"></div>
      <h4>Важным персонам</h4>
      <p>
        Вы опытный бизнесмен, который ищет себе партнёров по бизнесу?
        Зарегистрируйтесь у нас и найдите для себя единомышленников.
      </p>
    </div>
    <div class="small-12 medium-4 columns text-center">
      <div class="landing-img landing-img-4"></div>
      <h4>Желающим отдохнуть</h4>
      <p>
        У вас есть желание поиграть в футбол в своём районе, но больше нет желающих?
        Найдите себе участников в нашей сети и организуйте вместе с ними спортивное мероприятие.
      </p>
    </div>
  </div>

  <?php if (\Auth::check()) { ?>
      <a href="<?=route('profile', $user->id)?>" class="landing-register-btn button success">Ваша страница</a>
  <?php } else { ?>
      <a href="<?=route('register')?>" class="landing-register-btn button success">Зарегистрироваться</a>
  <?php } ?>
</div>