<form method="POST" action="/auth/register?nocache=1">
  <input type="hidden" name="_token" value="<?= csrf_token() ?>">

    <h1 class="form-signin-heading">Регистрация</h1>
    <br/>

    <fieldset>
      <legend>Основная информация</legend>
      <div class="row">
        <div class="large-8 columns large-offset-2">
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Пол</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?=Form::select('its_male', [
                '' => 'Пожалуйста, укажите свой пол',
                '1' => 'Мужчина',
                '0' => 'Женщина',
              ], old('its_male'))?>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Email</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?= Form::email('email', old('email'), [ 'class' => 'form-control', 'autofocus' => '']); ?>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Отображаемое имя</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?=Form::text('name', old('name'), [ 'class' => 'form-control' ])?>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Пароль</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?=Form::password('password', [ 'class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => '(8 знаков минимум)' ])?>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Подтверждение пароля</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?=Form::password('password_confirmation', [ 'class' => 'form-control', 'autocomplete' => 'off' ])?>
            </div>
          </div>
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Место проживания</label>
            </div>
            <div class="small-12 medium-9 columns">
              <?=Form::select('city', [], null, [ 'class' => '', 'data-placeholder' => "Введите название вашего города" ])?>
              <script>
                App.onFullLoad(function() {
                  yandex_selector('select[name="city"]');
                });
              </script>
            </div>
          </div>
        </div>
      </div>
    </fieldset>

    <fieldset>
      <legend>Дополнительная информация, необязательна к заполнению:</legend>
      <div class="row">
        <div class="large-8 columns large-offset-2">
          <div class="row">
            <div class="small-12 medium-3 columns">
              <label class="small-text-left medium-text-right inline">Телефон</label>
            </div>
            <div class="small-12 medium-9 columns">
              <input class="form-control" placeholder="+7(___) ___ __ __" type="tel" name="phone" id="<?=old('phone')?>">
              <script>
                App.onFullLoad(function() {
                    $("input[name='phone']").mask("+7(999) 999 99 99");
                });
              </script>
            </div>
          </div>
        </div>
      </div>
    </fieldset>

    <div class="row">
      <div class="small-8 small-offset-2 columns">
        <input type="submit" class="button success senddata" name="register_btn" value="Завершить регистрацию" />
      </div>
    </div>
</form>