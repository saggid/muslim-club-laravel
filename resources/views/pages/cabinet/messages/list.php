<?
use App\Helpers\Text;
use App\Models\User;

$showed_persons = [];
?>

<? foreach ($dialogs as $dialog)
{ ?>
    <div class="dialogs-container">
    <? $message_info = $dialog->getLastMessage(); ?>
    <? $person = User::find($dialog->user_to === $user->id ? $dialog->user_from : $dialog->user_to); ?>
    <? if (!in_array($person->id,$showed_persons)) { $showed_persons[] = $person->id; ?>
        <a href="<?= route('messages.dialog', $person->id) ?>" class="panel dialog">
            <h4 class="person"><?= $person->name() ?></h4>
            <div class="last-message"><?= Text::desc($message_info->message) ?></div>
            <div class="clearfix">
                <div class="last-message-date"><?= Text::humanDateTime($message_info->created_at) ?></div>
            </div>
        </a>
    <? } ?>
    </div>
<? } ?>

<? if ($dialogs->count() === 0) { ?>
<div class="panel">

    <p>В данный момент у вас еще нет ни одной беседы. Добавьте кого-нибудь в друзья и начните переписку.</p>

</div>
<? } ?>