<?
use App\Helpers\Text;
?>

<h1>Диалог с <?= $person->htmlLink() ?></h1>

<div class="dialog-messages">

    <div class="Aligner">
      <div class="Aligner-item Aligner-item--fixed">
        <?=$messages->appends(Input::except('page'))->render()?>
      </div>
    </div>
    <? foreach ($messages->reverse() as $message) { ?>
        <div class="message clearfix">
            <div class="content <?=$message->itsMy() ? 'right' : ''?>">
                <?= nl2br(trim($message->message)) ?>
                <div class="date has-tip"
                     data-tooltip aria-haspopup="true"
                     title="<?=Text::humanDate($message->created_at)?>"><?=$message->created_at->format('H:i')?></div>
            </div>
        </div>
    <? } ?>
    <div class="Aligner">
      <div class="Aligner-item Aligner-item--fixed">
        <?=$messages->appends(Input::except('page'))->render()?>
      </div>
    </div>

    <div class="new-message-form">
      <div class="clearfix">
        <textarea data-validation="notempty" name="message" placeholder="Текст вашего сообщения"></textarea>
      </div>

      <div data-input=".new-message-form" data-link="<?=route('messages.send',$person->id)?>" class="senddata button success">Отправить сообщение</div>

      <? /* Скроем основной контейнер описания ошибок, он тут не нужен */ ?>
      <div style="display:none"><div class="validation-errors"></div></div>
    </div>
</div>

<script>
    App.onFullLoad(function() {
        $('.new-message-form').on('keypress', function(event) {
            onEnter(event, function() {
                $('.new-message-form .senddata').click();
            });
        });
    });
</script>
