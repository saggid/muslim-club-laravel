<?php
use App\Helpers\Text;
?>
<div class="row profile-page">

    <div class="medium-7 columns">

        <h3>
          <?=$person->name()?><? if ($person->isCurrentUser()) { ?><small>, это вы</small><? } ?>
        </h3>
        <p class="user-description"><?=$person->city->name?>, <?=$person->city->region->name?>, <?=$person->city->region->country->name?></p>
        <a href="<?= route('messages.dialog', $person->id) ?>" class="clickable-dashed">
            <? if ($person->isCurrentUser()) { ?>
                Написать себе заметку
            <? } else { ?>
                Написать <?=sex($person,'ему','ей')?>
            <? } ?>
        </a>


        <dl class='dl-horizontal user-characteristics'>
            <dt>Зарегистрирован</dt>
            <dd><?=Text::humanDate($person->created_at)?></dd>
            <dt>
                Интересы<?php if ($person->isCurrentUser()) { ?> <a class="clickable-dashed" href="<?=route('cabinet.edit-hobbies')?>">указать</a><?php } ?>
            </dt>
            <dd>
                <?php foreach($person->hobbies as $hobby) { ?>
                    <span class="label"><?=$hobby->title?></span>
                <?php } ?>
                <?php if ($person->hobbies->count() === 0 AND ! $person->isCurrentUser()) { ?>
                    <p>ещё не указаны</p>
                <?php } ?>
            </dd>
            <?php if ($person->phone) { ?>
            <dt>Контакты</dt>
            <dd>
                <?php if ($person->phone) { ?>
                    <?=$person->phone?>
                <?php } ?>
            </dd>
            <?php } ?>
        </dl>



    </div>

    <div class='medium-5 columns'>

        <?php if ($person->about) { ?>
            <h3>О себе:</h3>
            <?=$person->about?>
        <?php } ?>

    </div>
</div>