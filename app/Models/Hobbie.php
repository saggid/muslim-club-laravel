<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hobbie extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hobbies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'title', 'description'];

    function __toString() {
        return $this->title;
    }

    function users() {
        return $this->belongsToMany('App\Models\Hobbie', 'users_hobbies', 'hobby_id', 'user_id');
    }

}
