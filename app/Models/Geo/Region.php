<?php

namespace App\Models\Geo;

use Illuminate\Database\Eloquent\Model;

class Region extends Model {

  // The database table used by the model.
  protected $table = 'geo_regions';
  protected $fillable = ['name','country_id'];

  function __toString() {
    return (string) ($this->exists) ? $this->name : 'Неизвестный регион';
  }

  function country() {
    return $this->belongsTo('App\Models\Geo\Country', 'country_id');
  }

  function cities() {
    return $this->hasMany('App\Models\Geo\City', 'region_id');
  }

  /**
   * Возвращает количество городов в регионе
   */
  function getCitiesCount() {
    return $this->cities()->remember(10)->count();
  }

}
