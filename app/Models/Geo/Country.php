<?php

namespace App\Models\Geo;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

  // The database table used by the model.
  protected $table = 'geo_countries';
  protected $fillable = ['name','iso'];

  function __toString() {
    return (string) ($this->exists) ? $this->name : 'Неизвестная страна';
  }

  function regions() {
    return $this->hasMany('App\Models\Geo\Region', 'country_id');
  }

  function cities() {
    return $this->hasMany('App\Models\Geo\City', 'country_id');
  }

}
