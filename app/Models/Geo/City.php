<?php

namespace App\Models\Geo;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

  // The database table used by the model.
  protected $table = 'geo_cities';
  protected $fillable = ['name','area','dolgota','shirota','region_id','country_id'];

  function __toString() {
    return (string) ($this->exists) ? $this->name : 'Неизвестный город';
  }

  function fullname() {
    $title = $this->name;
    if ($this->region_id) {
      $title .= ', ' . $this->region->name;
      if (trim($this->area) !== trim($this->region->name)) {
        $title .= ', ' . $this->area;
      }
    } elseif ($this->area) {
      $title .= ', ' . $this->area;
    }
    return "{$title}, {$this->country->name}";
  }

  function country() {
    return $this->belongsTo('App\Models\Geo\Country', 'country_id');
  }

  function region() {
    return $this->belongsTo('App\Models\Geo\Region', 'region_id');
  }
  
}