<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Michelf\Markdown;

class Message extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_from', 'user_to', 'message', 'readed_at' ];

    function from() {
        return $this->belongsTo('App\Models\User', 'user_from');
    }

    function to() {
        return $this->belongsTo('App\Models\User', 'user_to');
    }

    function itsMy() {
        $current_user = Auth::user();
        return $this->user_from === $current_user->id;
    }

    function getLastMessage() {
        $user_from = $this->user_from;
        $user_to   = $this->user_to;

        return (new self)
            //->select(['message','created_at'])
            ->where(function($query) use ($user_from, $user_to) {
                $query->where('user_from', $user_from)->where('user_to', $user_to);
            })
            ->orWhere(function($query) use ($user_from, $user_to) {
                $query->where('user_to', $user_from)->where('user_from', $user_to);
            })
            ->orderBy('created_at','desc')
            ->limit(1)
            ->first()
        ;
    }

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return string
     */
    public function setMessageAttribute($value)
    {
      $this->attributes['message'] = Markdown::defaultTransform(strip_tags($value));
    }

}
