<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'its_male', 'email', 'password', 'city_id', 'phone'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    function city() {
        return $this->belongsTo('App\Models\Geo\City', 'city_id');
    }

    function hobbies() {
        return $this->belongsToMany('App\Models\Hobbie', 'users_hobbies', 'user_id', 'hobby_id');
    }

    function isAdmin() {
        return ($this->exists AND $this->is_admin);
    }

    function __toString() {
        return $this->htmlLink();
    }

    function link() {
        return route('profile', $this->id);
    }

    function name() {
        return $this->name;
    }

    function htmlLink() {
        return "<a href='{$this->link()}'>{$this->name()}</a>";
    }

    /**
     * Возвращает True в том случае, если данная модель является текущим
     * авторизованным пользователем
     * @return boolean
     */
    function isCurrentUser() {
        return ($this->id === current_user()->id);
    }

}
