<?php

/**
 * В этом файле хранятся функции, упрощающие работу с массивами данных
 */

/**
* Retrieve a single key from an array. If the key does empty in the
* array, the default value will be returned instead.
*
* @param   array   array to extract from
* @param   string  key name
* @param   mixed   default value
* @return  mixed
*/
function array_get_empty($array, $key, $default = null) {
    return !empty($array[$key]) ? $array[$key] : $default;
}

/**
 * Сортировать массив объектов или других массивов по указанному свойству
 * @param array $array
 * @param string $prop наименование свойства
 */
function osort(&$array, $prop)
{
    usort(
        $array,
        function ($a, $b) use ($prop) {
            if (is_object($a)) {
                return $a->$prop > $b->$prop ? 1 : -1;
            } else {
                return $a[$prop] > $b[$prop] ? 1 : -1;
            }
        }
    );
}

/**
 * Поиск в массиве ассоциативных массивов (или объектов) тех,
 * в которых ключ соответствует значению
 * @param array $array Массив ассоциативных массивов или объектов
 * @param string $key_name Название ключа, по которому проверять
 * @param string $key_value Значение ключа, которое ищем
 * @return type
 */
function array_assoc_search($array, $key_name, $key_value)
{
    foreach ($array as $key => $item) {
        if (is_array($item)) {
            if ($item[$key_name] === $key_value) {
                return $key;
            }
        } elseif (is_object($item)) {
            if ($item->$key_name === $key_value) {
                return $key;
            }
        }
    }
    // Ничего не нашли, возвращаем FALSE
    return false;
}

/**
 * Возвратить элементы полученного массива через запятую
 * @param array $mass Массив данных
 * @param string $ifnull Возвращает это, если придет пустой массив
 * @param string $glue Что писать между элементами массива (по умолчанию ", ")
 * @return string
 */
function array_list($mass, $ifnull = '', $glue = ', ')
{
    if (empty($mass)) {
        return $ifnull;
    }
    return implode($glue, $mass);
}

/**
 * Возвратить указанные свойства массива через запятую
 * @param array $objects Массив данных
 * @param string $property Свойство, которое брать из объектов
 * @param string $ifnull Возвращает это, если придет пустой массив
 * @param string $glue Что писать между элементами массива (по умолчанию ", ")
 * @return string
 */
function array_list_by_property($objects, $property, $ifnull = '', $glue = ', ')
{
    return array_list(
        array_pluck($objects, $property),
        $ifnull,
        $glue
    );
}
