<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;
use Validator;

class Registrar implements RegistrarContract {

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return Validator
     */
    public function validator(array $data) {
        return Validator::make($data, [
                'name'     => 'required|max:255',
                'its_male' => 'required|in:1,0',
                'email'    => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:8',
                'city'     => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data) {
        $city = GeoObjects::make($data['city']);

        return User::create([
            'name'     => $data['name'],
            'its_male' => $data['its_male'],
            'city_id'  => $city->id,
            'phone'    => array_get($data, 'phone'),
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

}
