<?php

namespace App\Services;

use App\Models\Geo\City;
use App\Models\Geo\Country;
use App\Models\Geo\Region;

class GeoObjects {

    /**
     * Метод позволяет создать все необходимые географические объекты в базе проекта,
     * на основе информации, предоставленной API сервиса "Яндекс.Карты"
     * @param object $geo_data Объект с данными
     * @return City
     */
    static function make($geo_data) {
        
        if (empty($geo_data)) {
            // Если был передан пустой объект в качестве параметра,
            // просто вернём пустую модель города в качестве отказа от обработки.
            return (new City);
        }
        
        if (is_numeric($geo_data)) {
            // Если была передана цифра в качестве параметра - найдём указанный город по данной цифре
            return City::find($geo_data);
        }
        
        $geo_data = json_decode(htmlspecialchars_decode($geo_data));
        
        // Проверим на наличие все главные свойства объекта $geo_data
        foreach(['country', 'country_code', 'region', 'name', 'point'] as $property) {
            if (! property_exists($geo_data, $property)) {
                throw \Exception("Property {$property} in \$geo_data not exists.");
            }
        }
        
        $country = Country::firstOrCreate([
                'name' => $geo_data->country,
                'iso'  => $geo_data->country_code,
        ]);

        $region = Region::firstOrCreate([
                'name'       => $geo_data->region,
                'country_id' => $country->id,
        ]);

        $city = City::firstOrCreate([
                'name'       => $geo_data->name,
                'area'       => $geo_data->area,
                'dolgota'    => explode(' ', $geo_data->point)[0],
                'shirota'    => explode(' ', $geo_data->point)[1],
                'region_id'  => $region->id,
                'country_id' => $country->id,
        ]);
        
        return $city;
    }
    
}
