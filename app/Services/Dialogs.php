<?php

namespace App\Services;

use DB;

/**
 * Класс для работы с диалогами системы
 */
class Dialogs {

    /**
     * Возвращает список бесед указанного пользователя, отсортированных по последнему сообщению
     * @param type $user_id
     * @return type
     */
    function getList($user_id)
    {
        return (new \App\Models\Message)
            ->whereNull('deleted_at')
            ->where(function($query) use ($user_id) {
                $query->where('user_from', $user_id)->orWhere('user_to', $user_id);
            })
            ->groupBy('user_to', 'user_from', 'id')
            ->orderBy('created_at', 'desc')
            ->get()
        ;

//        return DB::table('messages')
//                ->select([
//                    'user_to',
//                    'user_from',
//                    DB::raw('(SELECT created_at FROM messages as cr_mes WHERE'
//                        . '(cr_mes.user_from = ? AND cr_mes.user_to = messages.user_to) OR (cr_mes.user_from = messages.user_from AND cr_mes.user_to = ?)'
//                        . 'ORDER BY created_at DESC LIMIT 1) '
//                        . 'as created_at_last_mes'),
//                    DB::raw('(SELECT message FROM messages as last_mes WHERE'
//                        . '(last_mes.user_from = ? AND last_mes.user_to = messages.user_to) OR (last_mes.user_from = messages.user_from AND last_mes.user_to = ?)'
//                        . 'ORDER BY created_at DESC LIMIT 1) '
//                        . 'as last_mes'),
//                ])->setBindings([ $user_id, $user_id, $user_id, $user_id])
//                ->distinct()
//                ->whereNull('deleted_at')
//                ->where(function($query) use ($user_id) {
//                    $query->where('user_from', $user_id)->orWhere('user_to', $user_id);
//                })
//                ->groupBy('user_to', 'user_from')
//                ->orderBy('created_at_last_mes', 'desc')
//                ->get()
//        ;

    }

    /**
     * Возвращает список сообщений с указанным пользователем
     * @param int $user_from
     * @param int $user_to
     * @param int $limit
     * @return array
     */
    function getMessages($user_from, $user_to, $limit = 10)
    {
        return (new \App\Models\Message)
                ->where(function($query) use ($user_to, $user_from) {
                    $query
                        ->where('user_from', $user_from)
                        ->where('user_to', $user_to);
                })
                ->orWhere(function($query) use ($user_to, $user_from) {
                    $query
                        ->where('user_to', $user_from)
                        ->where('user_from', $user_to);
                })
                ->orderBy('created_at', 'desc')
                ->paginate($limit)
        ;
    }

}
