<?php

namespace App\Http\Controllers;

use App\Models\User;

class Cabinet extends Controller {

    public function showProfile($user_id)
    {
        $view         = view('pages.cabinet');
        $view->person = User::findOrFail($user_id);
        $this->setContent($view, $view->person->name());
    }

}
