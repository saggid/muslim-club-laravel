<?php

namespace App\Http\Controllers;

class Landing extends Controller {

  /**
   * Show the application welcome screen to the user.
   *
   * @return Response
   */
  public function index()
  {
    $this->setContent(view('pages.landing')->render());
  }

}
