<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Http\Request;
use Response;

class Auth extends Controller {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The registrar implementation.
     *
     * @var Registrar
     */
    protected $registrar;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Guard  $auth
     * @param  Registrar  $registrar
     * @return void
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        parent::__construct();

        $this->auth      = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show the application registration form.
     * Handle a registration request for the application.
     *
     * @return Response
     */
    public function anyRegister(Request $request)
    {
        if (\Request::method() === 'GET')
        {
            $this->setContent(view('pages.auth.register'), 'Регистрация');
        } else
        {
            $validator = $this->registrar->validator($request->all());

            if ($validator->fails())
            {
                $this->throwValidationException(
                    $request, $validator
                );
            }

            $this->auth->login($this->registrar->create($request->all()));

            return [
                'redirect_full' => route('profile', $this->auth->user()->id)
            ];
        }
    }

    /**
     * Show the application login form.
     * Handle a login request to the application.
     *
     * @return Response
     */
    public function anyLogin(Request $request)
    {
        if (\Request::method() === 'GET')
        {
            $this->setContent(view('pages.auth.login'), 'Авторизация');
        } else
        {
            $this->validate($request, [
                'email'    => 'required|email',
                'password' => 'required',
            ]);

            $credentials = $request->only('email', 'password');

            if ($this->auth->attempt($credentials, $request->has('remember')))
            {
                $user = $this->auth->user();
                return redirect()->intended(route('profile', $user->id));
            }

            return redirect(route('login'))
                    ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'email' => $this->getFailedLoginMesssage(),
            ]);
        }
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMesssage()
    {
        return 'Введённые логин и пароль не являются верными.';
    }

    /**
     * Log the user out of the application.
     *
     * @return Response
     */
    public function getLogout()
    {
        $this->auth->logout();

        return redirect('/');
    }

}
