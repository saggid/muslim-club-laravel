<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\User as User;
use Request;
use Response;
use PrettyFormsLaravel\FormProcessLogic;

abstract class Controller extends BaseController {

  use DispatchesCommands,
      ValidatesRequests,
      FormProcessLogic;

  protected $user = null; /* @var $user User */
  protected $meta_title;
  protected $meta_description;
  protected $meta_keywords;

  /**
   * The layout that should be used for responses.
   */
  protected $layout = 'app';

  function __construct() {
    $this->meta_title       = config('seo.title');
    $this->meta_description = config('seo.description');
    $this->meta_keywords    = config('seo.keywords');

    $this->user = current_user();
  }

  /**
   * Execute an action on the controller.
   *
   * @param  string  $method
   * @param  array   $parameters
   * @return Response
   */
  public function callAction($method, $parameters) {
    $this->setupLayout();

    $response = call_user_func_array(array($this, $method), $parameters);

    // If no response is returned from the controller action and a layout is being
    // used we will assume we want to just return the layout view as any nested
    // views were probably bound on this view during this controller actions.
    if (is_null($response) AND ! is_null($this->layout)) {
      if (Request::isXmlHttpRequest()) {
        $response = $this->layout->content;
      } else {
        $response = $this->layout;
      }
    }

    return $response;
  }

  /**
   * Setup the layout used by the controller.
   *
   * @return void
   */
  protected function setupLayout() {
    $this->layout = view($this->layout);
    $this->layout->content = view('content');
  }

  /**
   * Установить контент и метаинформацию для страницы
   * @param View $html основной контент
   * @param string $title заголовок
   * @param string $description описание
   * @param string $keywords ключевые слова
   */
  protected function setContent($html, $title = null, $description = null, $keywords = null) {
    if (is_null($title))       { $title       = $this->meta_title; }
    if (is_null($description)) { $description = $this->meta_description; }
    if (is_null($keywords))    { $keywords    = $this->meta_keywords; }

    set_page_meta($title, $description, $keywords);
    $this->layout->content->html = $html;
  }

}
