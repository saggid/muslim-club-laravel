<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request as AppRequest;
use App\Http\Controllers\Controller;

class EditHobbies extends Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->middleware('auth');
    }

    protected $_model_name = 'App\Models\User';
    protected $fields      = [
        'hobbies' => [
            'tag'          => 'select-multi',
            'label'        => 'Интересы',
            'model'        => 'App\Models\Hobbie',
            'model_key'    => 'id',
            'model_name'   => 'title',
            'model_orders' => 'title',
        ],
    ];

    /**
     * Возвращает тексты, которые будут использоваться в генерации форм и сообщений для объекта
     * метод не обязательно создавать, по умолчанию класс будет использовать стандартные общие сообщения и заголовки
     */
    protected function getStrings($model) {
        return [
            'edit' => [
                'caption' => 'Редактирование ваших интересов',
                'success' => 'Список ваших интересов обновлён',
            ],
        ];
    }
    
    function getHomeLink($user) {
        return route('profile', $user->id);
    }

    function anySave(AppRequest $request) {
        if (\Request::wantsJson() AND \Request::isMethod('post')) {
            return $this->save($request, $this->user->id);
        } else {
            return $this->generateForm($this->user->id);
        }
    }

}