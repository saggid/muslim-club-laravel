<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request as AppRequest;
use Request;

class Messages extends Controller {

  private $dialogs = null;

  function __construct(\App\Services\Dialogs $dialogs) {
    parent::__construct();

    $this->dialogs = $dialogs;
    $this->middleware('auth');
  }

  protected $_model_name = 'App\Models\Message';
  protected $fields      = [
    'user_from' => [ 'tag' => 'hidden'],
    'user_to'   => [
      'tag'             => 'search',
      'label'           => 'Кому?',
      'model'           => 'App\Models\User',
      'ajax_url'        => '/search/users',
      'ajax_param_name' => 'name',
      'ajax_data_type'  => 'json',
      'display_as'      => 'name',
      'desc'            => 'Укажите пользователя',
    ],
    'message'   => [
      'tag'   => 'textarea',
      'label' => 'Текст',
    ],
  ];

  /**
   * Возвращает тексты, которые будут использоваться в генерации форм и сообщений для объекта
   * метод не обязательно создавать, по умолчанию класс будет использовать стандартные общие сообщения и заголовки
   */
  protected function getStrings($model) {
    return [
      'add' => [
        'caption' => 'Новое сообщение',
        'success' => 'Сообщение отправлено',
      ],
    ];
  }

  protected function getValidationRules($message) {
    return [
      'message' => 'required|min:1'
    ];
  }

  function getIndex() {
    $view          = view('pages.cabinet.messages.list');
    $view->dialogs = $this->dialogs->getList($this->user->id);
    $this->setContent($view, 'Беседы');
  }

  function getDialog($person_id) {
    $person = User::findOrFail($person_id);

    $view           = view('pages.cabinet.messages.dialog');
    $view->person   = $person;
    $view->messages = $this->dialogs->getMessages($this->user->id, $person->id, 10);
    $this->setContent($view->render(), 'Диалог с ' . $person->name());
  }

  function postSend(AppRequest $request, $person_id) {
    // Запретим писать сообщения несуществующим людям
    User::findOrFail($person_id);

    if (Request::wantsJson() AND Request::isMethod('post')) {
      $this->save($request, null, [
          'user_from' => $this->user->id,
          'user_to'   => $person_id,
      ]);

      return [ 'redirect' => route('messages.dialog', $person_id) ];

    } else {
      abort(404);
    }
  }

  function getHomeLink($message) {
    return route('messages.dialog', \Route::current()->parameter('one'));
  }

}
