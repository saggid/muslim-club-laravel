<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin;
use Input;
use Illuminate\Http\Request;

/**
 * Форма изменения пароля пользователя
 **/

class Password extends Admin {
    
    protected $_model_name = 'App\Models\User';
    
    protected $fields      = [
        'password'          => [
            'tag'        => 'input',
            'label'      => 'Новый пароль',
            'attributes' => ['data-validation' => 'notempty'],
        ],
    ];

	/**
	 * Возвращает тексты, которые будут использоваться в генерации форм и сообщений для объекта
	 * метод не обязательно создавать, по умолчанию класс будет использовать стандартные общие сообщения и заголовки
	*/
    protected function getStrings($model)
    {
        return [
            'edit' => [
				'caption' => 'Установка нового пароля',
				'success' => 'Пароль был успешно изменён',
			],
        ];
    }
    
    /**
     * После успешного сохранения, удаления и создания модели,
     * пользователь будет перенаправлен на URL, сгенерированный
     * данным методом
     */
    protected function getHomeLink($model) {
        return route('admin.users');
    }

    
  function anySet(Request $request) {
        if (\Request::wantsJson() AND \Request::isMethod('post')) {
            $password = bcrypt(Input::get('password'));
            return $this->save($request, pf_param(), [ 'password' => $password ]);
        } else {
            return $this->generateForm(pf_param(), [ 'password' => '' ]);
        }
  }
  
}
