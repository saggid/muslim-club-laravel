<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Models\Hobbie;
use Illuminate\Http\Request;

class Hobbies extends Admin {

    protected $_model_name = 'App\Models\Hobbie';
    protected $fields = [
        'title'       => [
            'tag'        => 'input',
            'label'      => 'Наименование',
            'attributes' => ['data-validation' => 'notempty'],
        ],
        'description' => [
            'tag'   => 'textarea',
            'label' => 'Описание',
        ],
    ];

    /**
     * Возвращает тексты, которые будут использоваться в генерации форм и сообщений для объекта
     * метод не обязательно создавать, по умолчанию класс будет использовать стандартные общие сообщения и заголовки
     */
    protected function getStrings($model) {
        return [
            'add'  => [
                'caption' => 'Новое хобби',
                'success' => 'Хобби создано',
            ],
            'edit' => [
                'caption' => 'Редактирование хобби',
                'success' => 'Хобби успешно обновлено',
            ],
        ];
    }

    /**
     * Правила валидации для текущего метода
     * @param object $model Модель, с которой мы работаем
     * @return array
     */
    protected function getValidationRules($model) {
        $except = $model->exists ? ",{$model->id}" : '';
        return [
            'title' => 'required|max:255|unique:hobbies,title' . $except,
        ];
    }  
  
  function getIndex() {
      $view = view('pages.admin.hobbies');
      $view->hobbies = Hobbie::withTrashed()->orderBy('id')->paginate(15);
      $this->setContent($view, 'Хобби');
  }
  
  function anySave(Request $request) {
      return $this->defaultSaveLogic($request);
  }

  function anyDelete() {
      return $this->defaultDeleteLogic();
  }
  
    function postRestore() {
        return $this->defaultRestoreLogic();
    }

    function postForceDelete() {
        return $this->defaultForceDeleteLogic();
    }

}
