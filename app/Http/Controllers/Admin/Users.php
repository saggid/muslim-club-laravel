<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin;
use App\Models\User;
use App\Services\GeoObjects;
use Input;
use Illuminate\Http\Request;

class Users extends Admin {

    protected $_model_name = 'App\Models\User';
    protected $fields      = [
        'email'    => [
            'tag'        => 'input',
            'label'      => 'E-Mail',
            'attributes' => ['data-validation' => 'notempty'],
        ],
        'phone'    => [
            'tag'   => 'input-phone',
            'label' => 'Номер телефона',
        ],
        'name'     => [
            'tag'        => 'input',
            'label'      => 'Имя',
            'attributes' => ['data-validation' => 'notempty'],
        ],
        'about'    => [
            'tag'   => 'editor',
            'label' => 'Немного о себе',
        ],
        'its_male' => [
            'tag'     => 'select',
            'label'   => 'Пол',
            'options' => [
                '1' => 'Мужской',
                '0' => 'Женский',
            ],
        ],
        'hobbies' => [
            'tag'          => 'select-multi',
            'label'        => 'Интересы',
            'model'        => 'App\Models\Hobbie',
            'model_key'    => 'id',
            'model_name'   => 'title',
            //'model_desc'   => 'description',
            //'model_orders' => 'title',
        ],
        'city_id'  => [
            'tag'                   => 'search',
            'label'                 => 'Город',
            'model'                 => 'App\Models\Geo\City',
            'ajax_url'              => '//geocode-maps.yandex.ru/1.x/?format=json',
            'ajax_param_name'       => 'geocode',
            'ajax_data_type'        => 'jsonp',
            'ajax_process_function' => 'yandex_process_results',
            'display_as'            => 'fullname',
        ],
        'is_admin' => [
            'tag'   => 'checkbox',
            'label' => 'Флаг администратора',
            'text'  => 'Пользователь является администратором',
        ],
    ];

    /**
     * Возвращает тексты, которые будут использоваться в генерации форм и сообщений для объекта
     * метод не обязательно создавать, по умолчанию класс будет использовать стандартные общие сообщения и заголовки
     */
    protected function getStrings($model) {
        return [
            'add'  => [
                'caption' => 'Новый пользователь',
                'success' => 'Пользователь успешно создан',
                'legend'  => 'Информация',
            ],
            'edit' => [
                'caption' => 'Редактирование пользователя',
                'success' => 'Пользователь успешно обновлён',
                'legend'  => 'Информация',
            ],
        ];
    }

    /**
     * Правила валидации для текущего метода
     * @param object $model Модель, с которой мы работаем
     * @return array
     */
    protected function getValidationRules($model) {
        $except = $model->exists ? ",{$model->id}" : '';
        return [
            'name'     => 'required|max:45',
            'email'    => 'required|email|max:45|unique:users,email' . $except,
            'city_id'  => 'required|exists:geo_cities,id',
            'its_male' => 'required|in:1,0',
        ];
    }

    function getIndex() {
        $view          = view('pages.admin.users');
        $view->persons = User::withTrashed()->orderBy('id')->paginate(15);
        $this->setContent($view, 'Пользователи');
    }

    function anySave(Request $request, $user_id) {
        if (\Request::wantsJson() AND \Request::isMethod('post')) {
            $city = GeoObjects::make(Input::get('city_id'));
            return $this->save($request, $user_id, [ 'city_id' => $city->id]);
        } else {
            return $this->generateForm($user_id);
        }
    }

    function anyDelete() {
        return $this->defaultDeleteLogic();
    }

    function postRestore() {
        return $this->defaultRestoreLogic();
    }

    function postForceDelete() {
        return $this->defaultForceDeleteLogic();
    }

}
