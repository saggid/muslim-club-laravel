<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Response;
use Input;

class Search extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Welcome Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders the "marketing page" for the application and
      | is configured to only allow guests. Like most of the other sample
      | controllers, you are free to modify or remove it as you desire.
      |
     */

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function getIndex() {
        $view          = view('pages.search.index');
        $view->persons = User::all();
        $this->setContent($view,'Поиск друзей');
    }

    function getUsers() {
        $users = [];
        $query = Input::get('name');
        $persons = User::where('name','ilike',"%{$query}%")->orWhere('email','like',"%{$query}%")->get();
        foreach ($persons as $person) {
            $users[] = [
                'id'   => $person->id,
                'text' => $person->name(),
            ];
        }
        return $users;
    }

}
