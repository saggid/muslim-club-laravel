<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;

class Admin extends Controller {

  /**
   * Create a new authentication controller instance.
   *
   * @param  Guard  $auth
   * @param  Registrar  $registrar
   * @return void
   */
  public function __construct() {
    parent::__construct();

    $this->middleware('auth');
    $this->middleware('admin');
  }
  
  function getIndex() {
      $this->setContent(view('pages.admin.index'));
  }

}
