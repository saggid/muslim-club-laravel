<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [ 'as' => 'landing', 'uses' => 'Landing@index']);

Route::get('user{user_id}', [ 'as' => 'profile', 'uses' => 'Cabinet@showProfile' ]);

Route::controller('cabinet/edit-hobbies', 'Cabinet\EditHobbies', [
    'anySave' => 'cabinet.edit-hobbies'
]);

Route::controller('cabinet/messages', 'Cabinet\Messages', [
    'getIndex'  => 'messages',
    'postSend'  => 'messages.send',
    'getDialog' => 'messages.dialog'
]);

// Всё, что связано в авторизацией и регистрацией
Route::controller('auth', 'Auth', [
    'anyLogin'    => 'login',
    'anyRegister' => 'register',
    'getLogout'   => 'logout',
]);

// Установка нового пароля для пользователя
Route::controller('admin/users/password', 'Admin\Users\Password', [ 'anySet' => 'admin.users.password' ]);

// Управление пользователями
Route::controller('admin/users', 'Admin\Users', [
    'getIndex' => 'admin.users',
    'anySave'  => 'admin.users.save',
]);

// Управление хобби
Route::controller('admin/hobbies', 'Admin\Hobbies', [
    'getIndex' => 'admin.hobbies',
    'anySave'  => 'admin.hobbies.save',
]);

Route::controller('admin', 'Admin', [
    'getIndex' => 'admin',
]);

Route::controller('search', 'Search', [
    'getIndex' => 'search',
]);

Route::controller('password','Auth\Password');