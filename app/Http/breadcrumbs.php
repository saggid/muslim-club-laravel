<?php

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Params;
use App\Models\User;

Breadcrumbs::register('admin', function($breadcrumbs)
{
    $breadcrumbs->push('Панель администратора', route('admin'));
});

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Главная', route('home'));
});

Breadcrumbs::register('profile', function($breadcrumbs, $user)
{
    if (is_string($user)) {
        $user = User::findOrFail($user);
    }
    $breadcrumbs->push($user->name(), route('profile', $user->id));
});

Breadcrumbs::register('cabinet.edit-hobbies', function($breadcrumbs)
{
    $user = current_user();
    $breadcrumbs->parent('profile', $user);
    $breadcrumbs->push('Изменение интересов', route('cabinet.edit-hobbies'));
});

Breadcrumbs::register('messages', function($breadcrumbs)
{
    $user = current_user();
    $breadcrumbs->parent('profile', $user);
    $breadcrumbs->push('Беседы', route('messages'));
});

Breadcrumbs::register('messages.dialog', function($breadcrumbs, $user_id)
{
    $user = User::findOrFail($user_id);
    $breadcrumbs->parent('messages');
    $breadcrumbs->push("Диалог с {$user->name()}", route('messages.dialog'));
});

$breadcrumbs_routes = [
	'admin.users' => [
		'parent'       => 'admin',
        'model'        => 'App\Models\User',
		'title'        => 'Пользователи',
		'title_create' => 'Новый пользователь',
		'title_edit'   => 'Редактирование пользователя: ',
	],
	'admin.users.password' => [
		'parent'       => 'admin.users',
        'model'        => 'App\Models\User',
		'title'        => function($user) { return "Установка пароля пользователю: {$user}"; },
	],
	'admin.hobbies' => [
		'parent'       => 'admin',
        'model'        => 'App\Models\Hobbie',
		'title'        => 'Хобби',
		'title_create' => 'Новое хобби',
		'title_edit'   => 'Редактирование хобби: ',
	],
];

// Список скрытых разделов с хлебными крошками, которые не надо отображать
$breadcrumbs_hidden = [ 'home', 'admin', 'profile' ];

$params = Params::instance();
$params->breadcrumbs_routes = $breadcrumbs_routes;
$params->breadcrumbs_hidden = $breadcrumbs_hidden;

// Сгенерируем крошки для объектов в админке. Генерация обработчиков крошек производится
// через обработку ассоциативного массива $breadcrumbs_routes
foreach($breadcrumbs_routes as $route_name => $route_info) {

	// Главная страница объекта
	Breadcrumbs::register($route_name, function($breadcrumbs, $parent_object = null) use ($route_name, $route_info, $breadcrumbs_routes) {


        // Если родительский объект - это не модель, значит попытаемся сделать его моделью
        if (!is_null($parent_object) AND !$parent_object instanceof Model) {
            if (!isset($route_info['model'])) {
                throw new ErrorException("Пожалуйста, укажите модель, с которой должна работать генерация крошек в правиле {$route_name}.");
            }
            $parent_object = $route_info['model']::find($parent_object);
        }

        $title_str = is_callable($route_info['title'])
            ? $route_info['title']($parent_object)
            : $route_info['title'];


        if ($parent_object instanceof Model) {

            if (isset($route_info['top_relation'])) {
                $top_relation = $route_info['top_relation'];
                $breadcrumbs->parent($route_info['parent'], $parent_object->$top_relation);
            } else {
                // Если родительский роут нужадется в модели, передадим ему её
                // Иначе - ничего передавать не будем, чтобы не засорять ссылку ненужной информацией
                if (    isset($breadcrumbs_routes[$route_info['parent']])
                    AND isset($breadcrumbs_routes[$route_info['parent']]['model']))
                {
                    $breadcrumbs->parent($route_info['parent'], $parent_object);
                } else {
                    $breadcrumbs->parent($route_info['parent']);
                }
            }

            $breadcrumbs->push($title_str, route($route_name,$parent_object->id));
        } else {
            $breadcrumbs->parent($route_info['parent']);
            $breadcrumbs->push($title_str, route($route_name));
        }

	});

    // Страница создания / изменения объекта
    if (isset($route_info['title_create']) AND isset($route_info['title_edit'])) {
        Breadcrumbs::register("{$route_name}.save", function($breadcrumbs, $model = null) use ($route_name, $route_info) {

            $parent_object = null;
            if (isset($route_info['model'])) {
                if (is_numeric($model)) {
                    $parent_object = $route_info['model']::find($model);
                } elseif (is_object($model) AND $model->exists) {
                    if (isset($route_info['parent_relation'])) {
                        $parent_relation = $route_info['parent_relation'];
                        $parent_object = $model->$parent_relation;
                    } else {
                        $parent_object = null;
                    }
                } else {
                    // Если родительский объект не был передан, значит мы хотим загрузить
                    // страницу создания объекта, следовательно, номер родительского
                    // объекта уже был передан в URL и по правилам должен идти вторым номером
                    // поэтому создадим родительскую модель на основе второго параметра в запросе
                    $parent_object = $route_info['model']::find(pf_param('two'));
                }

                $breadcrumbs->parent($route_name, $parent_object);

            } else {
                $breadcrumbs->parent($route_name, $model);
            }


            if (is_null($parent_object)) {
                $breadcrumbs->push($route_info['title_create'], route("{$route_name}.save"));
            } else {
                $edit_str = is_callable($route_info['title_edit'])
                    ? $route_info['title_edit']($model, $parent_object)
                    : $route_info['title_edit'] . (string) $parent_object;
                $breadcrumbs->push($edit_str, route("{$route_name}.save",$parent_object->id));
            }
        });
    }

}
