<?php

/**
 * Маленький сниппет кода, позволяющий анимировать появление определённого элемента на странице
 * @param string $selector CSS-селектор к нужному элементу
 * @param string $effect Вариант эффекта
 * @param boolean $shuffle Рандомизировать эффект?
 * @param int $delay Время паузы между активацией анимации к каждой букве
 */
function textillate($selector, $effect = 'fadeIn', $shuffle = false, $delay = 25) { ?>
    <style>
        <?=$selector?> {
            visibility: hidden;
        }
    </style>
    <script>
        App.onFullLoad(function() {
            $('<?=$selector?>').textillate({
                in: {
                    effect: '<?=$effect?>'
                    , shuffle: <?=$shuffle ? 'true' : 'false'?>
                    , delay: <?=$delay?>
                }
            });
        });
    </script>
<?php }


/**
 * Вернуть текст для мужчины или женщины, в зависимости от пола указанного пользователя
 * @param \App\Models\User $user
 * @param string $man
 * @param string $woman
 * @return type
 */
function sex($user, $man, $woman) {
    return ($user->its_male) ? $man : $woman;
}