Navigation = new function () {

    this.ajaxObj = '';			// Объект ajax-загрузки
    this.content = '#content';	// Объект, содержащий в себе весь HTML-контент страницы

    this.showTimeout = null;

    this.ee = EventEmitter.getInstance(); // EventEmitter Object
    this.pages = {}; // Кеш загруженных страниц

    this.history = {};

    // Подсветим нужные ссылки и уберем подсветку с ненужных
    this.setActiveSection = function (name) {
        $('#menu a').removeClass('button-active');
        $('#menu #menu-' + name).addClass('button-active');
    };

    this.reloadFullPage = function (link) {
        if (empty(link)) {
            link = document.location.href;
        }
        document.location.href = link;
    };

    this.refreshPage = function () {
        this.loadPage(document.location.href,true);
    };

    /**
     * Произвести "мягкую" перезагрузку текущей страницы, без её полной перезагрузки
     */
    this.ajaxReload = function() {
        alert('функция аяк-перезагрузки не сделана. Сделай её, Роман, если в ней есть нужда.');
        /*this.refreshPage();
        $('#ajax_form_reg').attr('data-loaded',null).load('/_etc/loginform');
        this.updateCoreInfo();*/
    };

    // Установить новый контент на странице
    this.setContent = function (content) {
        this.ee.emit('beforeSetContent');
        this.content.html(content);

        $(document).foundation('tooltip', 'reflow');

        // Если cсылка содержит еще и якорь, то направим страницу туда
        if (location.hash) {
            this.scrollToAnkor(location.hash);
        } else {
            // Быть может, на странице есть элемент с атрибутом автофокусировки?
            var autofocus_element = this.content.find('[autofocus]').first();
            if (autofocus_element.length) {
                Navigation.scrollToElement(autofocus_element, 0);
                autofocus_element.focus();
            }
            //$(document).stop().scrollTo(100,200,'linear');
        }

        this.ee.emit('afterSetContent');
    };

    // Установим новую страницу
    this.setPage = function (link, html_data, old_history_page) {
        // Расскажем Гуглу о том, что мы загрузили новую страницу
        if (typeof(_gaq) !== 'undefined') {
            _gaq.push(['_trackPageview', link]);
            _gaq.push(['_trackEvent', 'Forms', 'AJAX Form']);
        }

        // Расскажем Яндекс-Метрике о том, что мы загрузили новую страницу
        if (typeof(yaCounter25127849) !== 'undefined') {
            yaCounter25127849.hit(link);
        }

        // Обновим счетчик ЛайвИнтернет
        if (typeof(updateLiveInternetCounter) !== 'undefined') {
            updateLiveInternetCounter();
        }

        // Если браузером поддерживается хистори, используем это
        if (supports_history_api()) {
            if (typeof(window.history.state) === 'number' && old_history_page !== true) {
                this.history[window.history.state].scroll = document.documentElement.scrollTop;
            }

            var time = new Date().getTime();
            this.history[time] = {
                html    : html_data,
                scroll  : 0
            };

            if (old_history_page !== true) {
                history.pushState(time, null, link);
            } else {
                history.replaceState(time, null, document.location.href);
            }
        }

        this.setContent(html_data, link);
    };

    /*
     * Функция загружает контент с сервера.
     * str      куда отправлсять запрос
     * mess     данные, которые следует передать серверу. Необязательно.
     * */
    this.loadPage = function (http_link, old_history_page) {
        var that = this;

        // Если уже был отправлен какой-либо запрос, отменим его
        if (typeof(this.ajaxObj) === 'object') this.ajaxObj.abort();

        http_link = '/' + http_link.replace('http://'+location.hostname+'/', '').replace('https://'+location.hostname+'/', '').replace('//', '');

        // Оповестим всех о начале загрузки страницы
        this.ee.emit('PageLoadingStart', http_link);

        this.ajaxObj = jQuery.ajax({
            type: "GET",
            url: http_link,
            dataType: 'html',
            success: function (data) {
                that.ee.emit('PageLoadingSuccess', http_link);
                that.setPage(http_link, data, old_history_page);
            },
            error: function (data, status, e) {
                if (e !== 'abort') {
                    that.ee.emit('PageLoadingError', http_link);
                    var errm = '<h3>Ошибка загрузки страницы</h3><p>Причина: ' + e + '</p><p>Попробуйте <span class="a-like" onclick="Navigation.reloadFullPage(); return false;">обновить страницу</span></p>';
                    that.setPage(http_link, errm);
                } else {
                    that.ee.emit('PageLoadingAborted', http_link);
                }
            }
        });

        if (http_link.lastIndexOf('#') === -1)
            return true;
        else
            return false;
    };

    this.setTitle = function (name) {
        document.title = name;
    };

    this.scrollToAnkor = function (aname) {
        var link = jQuery('a[name=' + aname + ']').get(0);
        if (typeof(link) === 'object') {
            var linkcoords = getElementPosition(link);
            $.scrollTo(linkcoords.top, 0);
        }
    };

    this.scrollToElement = function (el, speed, offset) {
        if (typeof(offset) === 'undefined') { offset = 0; }
        el = $(el).get(0);
        if (typeof(el) !== 'undefined') {
            var linkcoords = getElementPosition(el);
            if (typeof(speed) === 'undefined') {
                speed = 300;
            }

            $.scrollTo(parseInt(linkcoords.top - 20 + offset), {
                duration: speed,
                easing : 'linear',
                axis: 'y'
            });
        }
    };

};

App.onFullLoad(function() {

    Navigation.left = jQuery(Navigation.left);
    Navigation.content = jQuery(Navigation.content);

    // Нажатие на ссылки
    $('body').on('click', 'a', function () {
        var $this = jQuery(this);
        var cleared_href = this.href.substring(0, this.href.lastIndexOf('#'));
        var cleared_lasturl = document.location.href.substring(0, document.location.href.lastIndexOf('#'));
        var link_host = this.href.replace(/https?:\/\/([^\/]+)(.*)/, '$1');

        var href = this.href; //.replace('http:','').replace('https:','');

        // Если в ссылке есть якорь, и ссылка является точно такой же, как текущая страница, то просто переместим страницу на нужный якорь
        if ((href.indexOf('#') !== -1) && (cleared_lasturl === cleared_href)) {
            Navigation.scrollToAnkor(href);
            return false;
        } else {
            if (($this.attr('target') !== '_blank')
                && (href[href.length - 1] !== '#')
                && (!$this.hasClass('showmessage'))
                && (!$this.hasClass('senddata'))
                && (!$this.hasClass('senddata-token'))
                && (link_host == location.hostname) // Предотвращает попытки скачать контент с другого хоста
                && (this.getAttribute('data-navigation') !== 'base')
                && (!$this.parent().hasClass('cke_button')))
            {
                if ($this.attr('onclick')) {
                    return false;
                } else {
                    //Message.close();
                    Navigation.loadPage(href);
                    return false;
                }
            }

            if (href[href.length - 1] === '#') {
                return false;
            }

            return true;
        }
    });

});

// Сохраним контент текущей страницы, чтобы не загружать его повторно позже
if (supports_history_api() && isSessionStorageAvailable()) {
    var time = new Date().getTime();
    history.replaceState(time, null, document.location.href);
    Navigation.history[time] = {
        html    : $('#content').html(),
        scroll  : 0
    };
}


function supports_history_api() {
    return !!(window.history && history.pushState);
};

function isSessionStorageAvailable() {
    try {
        return 'sessionStorage' in window && window['sessionStorage'] !== null;
    } catch (e) {
        return false;
    }
}

// Узнать координаты элемента
function getElementPosition(elem) {
    //var elem = document.getElementById(elemId);

    var w = elem.offsetWidth;
    var h = elem.offsetHeight;

    var l = 0;
    var t = 0;

    while (elem) {
        l += elem.offsetLeft;
        t += elem.offsetTop;
        elem = elem.offsetParent;
    }

    return {
        "left": l,
        "top": t,
        "width": w,
        "height": h
    };
}

if (supports_history_api()) {
    window.addEventListener('popstate', function (e) {
        var time = window.history.state;
        if (typeof(history.state) === 'number' && typeof(Navigation.history[time]) === 'object') {
            var html =  Navigation.history[time].html;
            var scroll = Navigation.history[time].scroll;

            Navigation.setContent(html);
            window.scrollTo(0, scroll);
        } else {
            Navigation.loadPage(document.location.href,true);
        }

    }, false);
}

App.onFullLoad(function() {
    PrettyForms.Commands.registerHandler('redirect', function(uri) {
        Navigation.loadPage(uri);
    });

    PrettyForms.Commands.registerHandler('redirect_full', function (link) {
        document.location.href = link;
    });

    PrettyForms.Commands.registerHandler('refresh', function() {
        Navigation.refreshPage();
    });

});