App = new function () {

    // Выполнить функцию тогда, когда страница будет полностью загружена
    this.onFullLoad = function(func) {
        if (this.onFullLoadInited) {
            func();
        } else {
            this.onFullLoadFuncs.push(func);
        }
    };
    this.onFullLoadFuncs = [];
    this.onFullLoadInited = false;

    this.fullLoad = function() {
        this.onFullLoadFuncs.map(function(el){ el(); });
        this.onFullLoadInited = true;
    };

};

// При первой загрузке страницы выполним некоторые важные для корректной инициализации сайта действия
App.onFullLoad(function() {

    $(document).foundation();

    // Шаблон контейнера ошибки валидации для фреймворка Foundation
    PrettyForms.templates.element_validations_container
        = '<div style="display:none;margin-bottom:10px" id="validation-error-{%}" class="label radius secondary validation-error-container"></div>'
    ;
    PrettyForms.templates.element_validation_message
        = '<p style="margin-bottom:0;"><i class="fa fa-exclamation"></i>&nbsp;{%}</p>'
    ;

});