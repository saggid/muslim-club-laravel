function escapeHtml(string) {

  var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

/**
 * Функция возвращает все необходимые опции для плагина Ajax Bootstrap Select
 * для того, чтобы настроить его на получение данных с API Яндекс.Карт
 */
function yandex_selector(element) {
  $(element).select2({
    ajax: {
      url: "//geocode-maps.yandex.ru/1.x/",
      dataType: 'jsonp',
      delay: 250,
      data: function (params) {
        return {
            geocode: params.term
          , format: 'json'
        };
      },
      processResults: function (data) {
        return {
          results: yandex_process_results(data)
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 3
    //, templateResult: formatRepo // omitted for brevity, see the source of this page
    //, templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
}

function yandex_process_results(data) {
    var results = [];
    var yandexResponse = data.response;
    if (yandexResponse.GeoObjectCollection.featureMember.length > 0) {
      yandexResponse.GeoObjectCollection.featureMember.forEach(function (el) {
        var address = el.GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails;
        var region = address.Country.AdministrativeArea ? address.Country.AdministrativeArea.AdministrativeAreaName : '';
        var area = (region && address.Country.AdministrativeArea.SubAdministrativeArea) ? address.Country.AdministrativeArea.SubAdministrativeArea.SubAdministrativeAreaName : '';
        var point_data = el.GeoObject.Point.pos;
        results.push({
          id : escapeHtml(JSON.stringify({
              country: address.Country.CountryName
              , country_code: address.Country.CountryNameCode
              , region: region
              , area: area
              , name: el.GeoObject.name
              , point: point_data
            })),
          text: el.GeoObject.metaDataProperty.GeocoderMetaData.text
        });
      });
    }
    return results;
}

/* Создать редактор на ID элемента */
function editor(element, toolbar) {
  if (typeof (toolbar) === 'undefined')
    toolbar = 'basic';

  if (CKEDITOR.instances[element]) {
    delete CKEDITOR.instances[element];
  }

  CKEDITOR.replace(element, {
    allowedContent: true,
    toolbar: toolbar,
    toolbar_basic: [
      {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'NewPage', '-', 'Templates']},
      {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
      {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
      {name: 'insert', items: ['Table', 'HorizontalRule', 'SpecialChar', 'PageBreak']},
      {name: 'image', items: ['Image', 'FlashUpload', 'base64image']},
      {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
      {name: 'colors', items: ['TextColor', 'BGColor']},
      {name: 'tools', items: ['Maximize', 'ShowBlocks']},
      {name: 'others', items: ['-']}
    ],
    toolbar_full: [
      {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'NewPage', '-', 'Templates']},
      {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
      {name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
      {name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']},
      '/',
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Language']},
      {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
      {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak']},
      '/',
      {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
      {name: 'colors', items: ['TextColor', 'BGColor']},
      {name: 'tools', items: ['Maximize', 'ShowBlocks']},
      {name: 'others', items: ['-']}
    ]
  });

  CKEDITOR.config.height = 300;
}

function onEnter(ev, handler) {
    ev = ev || window.event;
    //console.log(ev);
    if (!ev.ctrlKey && !ev.shiftKey && ev.keyCode == 13) {
        ev.preventDefault();
        handler(ev.currentTarget);
    }
}

function onCtrlEnter(ev, handler) {
    ev = ev || window.event;
    if (ev.keyCode === 10 || ev.ctrlKey && ev.keyCode === 13) {
        handler();
    }
}